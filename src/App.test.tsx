import { shallow } from 'enzyme';
import React from 'react';
import renderer from 'react-test-renderer';

import App from './App';

describe('App', () => {
  it('should be truthy', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
