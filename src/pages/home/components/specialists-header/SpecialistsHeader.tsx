import React, { FC, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  ISpecialist,
  searchFilterSelector,
  SpecialistsListStates,
  specialistsListStateSelector,
} from '@pages/home/state';
import homeActions from '@pages/home/state/actions';

import SpecialistsActions from './components/specialists-actions';
import SpecialistsTitle from './components/specialists-title';
import './SpecialistsHeader.scss';

interface IOwnProps {
  specialists: ISpecialist[];
}

const SpecialistsHeader: FC<IOwnProps> = ({ specialists }: IOwnProps) => {
  const dispatch = useDispatch();
  const listState = useSelector(specialistsListStateSelector);
  const searchFilter = useSelector(searchFilterSelector);

  const toggleListClickHandler = useCallback(
    (newListState: SpecialistsListStates): void => {
      if (listState !== newListState) {
        dispatch(homeActions.setListState(newListState));
      }
    },
    [dispatch, listState],
  );

  const searchFilterChangeHandler = useCallback(
    (searchValue: string): void => {
      dispatch(homeActions.setSearchFilter(searchValue));
    },
    [dispatch],
  );

  return (
    <div className="header-container">
      <SpecialistsTitle
        listState={listState}
        specialistsAmount={specialists.length}
      />
      <SpecialistsActions
        listState={listState}
        searchFilter={searchFilter}
        toggleListClickHandler={toggleListClickHandler}
        searchFilterChangeHandler={searchFilterChangeHandler}
      />
    </div>
  );
};

export default SpecialistsHeader;
