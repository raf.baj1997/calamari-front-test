import { SpecialistsHeaderTexts } from '@pages/home/components/specialists-header/models';
import { SpecialistsListStates } from '@pages/home/state';

export const getSpecialistsTitleText = (
  state: SpecialistsListStates,
  specialistsAmount: number,
): string => {
  const prefix =
    state === SpecialistsListStates.ALL_FAVORITE
      ? SpecialistsHeaderTexts.FAVORITE_SPECIALISTS
      : SpecialistsHeaderTexts.MY_SPECIALISTS;
  return `${prefix} (${specialistsAmount})`;
};
