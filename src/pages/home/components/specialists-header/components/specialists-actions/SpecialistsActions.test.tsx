import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import { SpecialistsListStates } from '@pages/home/state';

import SpecialistsActions from './SpecialistsActions';

const LIST_STATE_MOCK = SpecialistsListStates.ALL_FAVORITE;
const SEARCH_FILTER_MOCK = 'Andrew';
const TOGGLE_LIST_CLICK_HANDLER_MOCK = (_state: SpecialistsListStates) => {};
const SEARCH_FILTER_CHANGE_HANDLER_MOCK = (_val: string) => {};

describe('SpecialistsActions', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <SpecialistsActions
        listState={LIST_STATE_MOCK}
        searchFilter={SEARCH_FILTER_MOCK}
        toggleListClickHandler={TOGGLE_LIST_CLICK_HANDLER_MOCK}
        searchFilterChangeHandler={SEARCH_FILTER_CHANGE_HANDLER_MOCK}
      />
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
