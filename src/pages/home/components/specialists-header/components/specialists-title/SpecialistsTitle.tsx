import React, { FC, memo } from 'react';

import { SpecialistsListStates } from '@pages/home/state';

import { getSpecialistsTitleText } from '../../utils';

import './SpecialistsTitle.scss';

interface IOwnProps {
  specialistsAmount: number;
  listState: SpecialistsListStates;
}

const SpecialistsTitle: FC<IOwnProps> = ({
  specialistsAmount,
  listState,
}: IOwnProps) => {
  const titleText = getSpecialistsTitleText(listState, specialistsAmount);
  return <span className="title">{titleText}</span>;
};

export default memo(SpecialistsTitle);
