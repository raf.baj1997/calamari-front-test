import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import { SpecialistsListStates } from '@pages/home/state';

import SpecialistsTitle from './SpecialistsTitle';

const LIST_STATE_MOCK = SpecialistsListStates.ALL_FAVORITE;
const VOTES_AMOUNT_MOCK = 7;

describe('SpecialistsTitle', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <SpecialistsTitle
        listState={LIST_STATE_MOCK}
        specialistsAmount={VOTES_AMOUNT_MOCK}
      />
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
