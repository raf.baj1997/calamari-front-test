export enum SpecialistsHeaderTexts {
  FAVORITE_SPECIALISTS = 'Favorite Specialists',
  ALL_FAVORITE = 'All Favorite',
  MY_SPECIALISTS = 'My Specialists',
  SEARCH_ICON_ALT = 'Search',
  SEARCH_ICON_PLACEHOLDER = 'Search...',
}
