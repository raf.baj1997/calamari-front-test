import React, { FC } from 'react';

import SimpleTooltip from '@shared/components/informers/simple-tooltip';

import { getDoctorTooltipText } from '../../utils';

import './DoctorLikeHeader.scss';

interface IOwnProps {
  isFavorite: boolean;
  likeClickHandler: () => void;
}

const DoctorLikeHeader: FC<IOwnProps> = ({
  isFavorite,
  likeClickHandler,
}: IOwnProps) => {
  const tooltipText = getDoctorTooltipText(isFavorite);
  return (
    <div className="doctor-like-header-container">
      <div className="doctor-like-header-container__more">
        <div className="doctor-like-header-container__more__icon" />
      </div>
      <SimpleTooltip
        className="doctor-like-header-container__like-container"
        render={tooltipText}
      >
        <button type="button" onClick={likeClickHandler}>
          <div className="doctor-like-header-container__like-container__icon" />
        </button>
      </SimpleTooltip>
    </div>
  );
};

export default DoctorLikeHeader;
