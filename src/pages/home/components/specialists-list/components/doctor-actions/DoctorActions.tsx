import React, { FC, memo } from 'react';

import ButtonGroup from '@shared/components/buttons/button-group';
import SimpleButton from '@shared/components/buttons/simple-button';

import { SpecialistsListTexts } from '../../models';

import './DoctorActions.scss';

const DoctorActions: FC = () => {
  return (
    <ButtonGroup additionalClass="doctor-actions">
      <SimpleButton additionalClass="doctor-actions__button">
        {SpecialistsListTexts.PROFILE}
      </SimpleButton>
      <SimpleButton additionalClass="doctor-actions__button">
        {SpecialistsListTexts.BOOK_VISIT}
      </SimpleButton>
    </ButtonGroup>
  );
};

export default memo(DoctorActions);
