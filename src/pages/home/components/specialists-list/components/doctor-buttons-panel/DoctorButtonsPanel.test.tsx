import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import DoctorButtonsPanel from './DoctorButtonsPanel';

describe('DoctorButtonsPanel', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <DoctorButtonsPanel />;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
