import React, { FC, memo } from 'react';

import ButtonGroup from '@shared/components/buttons/button-group';
import IconButton from '@shared/components/buttons/icon-button';

import './DoctorButtonsPanel.scss';

const DoctorButtonsPanel: FC = () => {
  return (
    <ButtonGroup additionalClass="doctor-btn-panel">
      <IconButton additionalClass="doctor-btn-panel__bell-icon-btn">
        <div className="doctor-btn-panel__bell-icon-btn__icon" />
      </IconButton>
      <IconButton additionalClass="doctor-btn-panel__calendar-icon-btn">
        <div className="doctor-btn-panel__calendar-icon-btn__icon" />
      </IconButton>
      <IconButton additionalClass="doctor-btn-panel__message-icon-btn">
        <div className="doctor-btn-panel__message-icon-btn__icon" />
      </IconButton>
    </ButtonGroup>
  );
};

export default memo(DoctorButtonsPanel);
