import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import DoctorRating from './DoctorRating';

const RATING_MOCK = {
  value: 4.2,
  votesAmount: 123,
};
const RATING_CHANGE_HANDLER_MOCK = (_val: number) => {};

describe('DoctorRating', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <DoctorRating
        rating={RATING_MOCK}
        ratingChangeHandler={RATING_CHANGE_HANDLER_MOCK}
      />
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
