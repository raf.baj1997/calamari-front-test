import { round } from 'lodash';
import React, { FC, memo } from 'react';

import { displayNumberWithComma } from '@shared/utils/stringOperations';

import './DoctorRatingInfo.scss';

interface IOwnProps {
  ratingValue: number;
  votesAmount: number;
}

const DoctorRatingInfo: FC<IOwnProps> = ({
  ratingValue,
  votesAmount,
}: IOwnProps) => {
  const roundedValue = round(ratingValue, 1);
  const ratingWithComma = displayNumberWithComma(roundedValue);
  return (
    <div className="doctor-rating-info-container">
      <span className="doctor-rating-info-container__rating-value">
        {ratingWithComma}
      </span>
      <span className="doctor-rating-info-container__votes-amount">
        {`(${votesAmount})`}
      </span>
    </div>
  );
};

export default memo(DoctorRatingInfo);
