import React, { FC } from 'react';

import SimpleRating from '@shared/components/inputs/simple-rating';
import { IRating } from '@shared/models/common';

import DoctorRatingInfo from './components/doctor-rating-info';
import './DoctorRating.scss';

interface IOwnProps {
  rating: IRating;
  ratingChangeHandler: (value: number) => void;
}

const DoctorRating: FC<IOwnProps> = ({
  rating,
  ratingChangeHandler,
}: IOwnProps) => {
  const { value, votesAmount } = rating;
  return (
    <div className="doctor-rating-container">
      <SimpleRating ratingValue={value} onChange={ratingChangeHandler} />
      <DoctorRatingInfo ratingValue={value} votesAmount={votesAmount} />
    </div>
  );
};

export default DoctorRating;
