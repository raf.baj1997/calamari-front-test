import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import { SPECIALISTS_MOCK } from '@pages/home/state/mocks';

import DoctorInfo from './DoctorInfo';

const SPECIALIST_MOCK = SPECIALISTS_MOCK[0];

describe('DoctorInfo', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <DoctorInfo specialist={SPECIALIST_MOCK} />;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
