import React, { FC, memo } from 'react';

import { ISpecialist } from '@pages/home/state';

import RoundedAvatar from '@shared/components/images/rounded-avatar';

import { getFullName, getSpecialistInitials } from '../../utils';

import './DoctorInfo.scss';

interface IOwnProps {
  specialist: ISpecialist;
}

const DoctorInfo: FC<IOwnProps> = ({ specialist }: IOwnProps) => {
  const initials = getSpecialistInitials(specialist);
  const fullName = getFullName(specialist);
  return (
    <div className="doctor-info-container">
      <RoundedAvatar avatar={specialist.avatar} initials={initials} />
      <span className="doctor-info-container__name">{fullName}</span>
      <span className="doctor-info-container__profession">
        {specialist.profession}
      </span>
    </div>
  );
};

export default memo(DoctorInfo);
