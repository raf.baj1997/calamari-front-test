export enum SpecialistsListTexts {
  REMOVE_FROM_FAVORITES = 'Remove from favorites',
  ADD_TO_FAVORITES = 'Add to favorites',
  PROFILE = 'Profile',
  BOOK_VISIT = 'Book a visit',
}
