import React, { FC, useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { ISpecialist } from '@pages/home/state';
import homeActions from '@pages/home/state/actions';

import SimpleCard from '@shared/components/cards/simple-card';
import SimpleHorizontalDivider from '@shared/components/dividers/simple-horizontal-divider';

import DoctorActions from './components/doctor-actions';
import DoctorButtonsPanel from './components/doctor-buttons-panel';
import DoctorInfo from './components/doctor-info';
import DoctorLikeHeader from './components/doctor-like-header';
import DoctorRating from './components/doctor-rating';
import './SpecialistsList.scss';

interface IOwnProps {
  specialists: ISpecialist[];
}

const SpecialistsList: FC<IOwnProps> = ({ specialists }: IOwnProps) => {
  const dispatch = useDispatch();

  /** dispatch changes only when we change store, so in our scenario it does not change. There is no need to add it
   * to a dependency Array. However, eslint does not recognize it because useDispatch is custom redux's hook and I added it to
   * dependency array because of that. */
  const prepareRatingChangeHandler = useCallback(
    (specialistId: string) => (ratingValue: number): void => {
      dispatch(homeActions.rateSpecialist({ ratingValue, specialistId }));
    },
    [dispatch],
  );

  const prepareLikeSpecialistHandler = useCallback(
    (specialistId: string) => (): void => {
      dispatch(homeActions.toggleIsFavorite(specialistId));
    },
    [dispatch],
  );

  return (
    <div className="specialists-container">
      {specialists.map((specialist) => {
        return (
          <SimpleCard
            key={specialist.id}
            additionalClass="specialists-container__hover-card"
          >
            <DoctorLikeHeader
              isFavorite={specialist.isFavorite}
              likeClickHandler={prepareLikeSpecialistHandler(specialist.id)}
            />
            <DoctorInfo specialist={specialist} />
            <DoctorButtonsPanel />
            <SimpleHorizontalDivider />
            <DoctorRating
              rating={specialist.rating}
              ratingChangeHandler={prepareRatingChangeHandler(specialist.id)}
            />
            <DoctorActions />
          </SimpleCard>
        );
      })}
    </div>
  );
};

export default SpecialistsList;
