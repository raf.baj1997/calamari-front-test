import { shallow } from 'enzyme';
import { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import { withStoreComponent } from '@shared/utils/testHelpers';

import Home from './Home';

describe('Home', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = withStoreComponent(Home);
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
