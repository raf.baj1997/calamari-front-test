import { put, takeLatest } from 'redux-saga/effects';

import homeActions from './actions';
import { SET_LIST_STATE } from './models';

function* resetSearchFilterSaga(): Generator {
  yield put(homeActions.setSearchFilter(''));
}

export function* setListStateSaga(): Generator {
  yield takeLatest(SET_LIST_STATE, resetSearchFilterSaga);
}
