import {
  IRateSpecialist,
  IRateSpecialistPayload,
  ISetListState,
  ISetSearchFilter,
  IToggleIsFavorite,
  RATE_SPECIALIST,
  SET_LIST_STATE,
  SET_SEARCH_FILTER,
  SpecialistsListStates,
  TOGGLE_IS_FAVORITE,
} from './models';

const setSearchFilter = (searchValue: string): ISetSearchFilter => ({
  type: SET_SEARCH_FILTER,
  payload: searchValue,
});

const setListState = (newListState: SpecialistsListStates): ISetListState => ({
  type: SET_LIST_STATE,
  payload: newListState,
});

const rateSpecialist = (payload: IRateSpecialistPayload): IRateSpecialist => ({
  type: RATE_SPECIALIST,
  payload,
});

const toggleIsFavorite = (specialistId: string): IToggleIsFavorite => ({
  type: TOGGLE_IS_FAVORITE,
  payload: specialistId,
});

export default {
  setSearchFilter,
  setListState,
  rateSpecialist,
  toggleIsFavorite,
};
