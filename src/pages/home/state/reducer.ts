import { SPECIALISTS_MOCK } from './mocks';
import {
  HomeActionTypes,
  ISpecialist,
  RATE_SPECIALIST,
  SET_LIST_STATE,
  SET_SEARCH_FILTER,
  SpecialistsListStates,
  TOGGLE_IS_FAVORITE,
} from './models';
import { updateSpecialistIsFavorite, updateSpecialistRating } from './utils';

export interface ISpecialistsState {
  list: ISpecialist[];
  listState: SpecialistsListStates;
  search: string;
}

export const SPECIALISTS_INITIAL_STATE: ISpecialistsState = {
  list: SPECIALISTS_MOCK,
  listState: SpecialistsListStates.ALL_FAVORITE,
  search: '',
};

export const specialistsReducer = (
  state = SPECIALISTS_INITIAL_STATE,
  action: HomeActionTypes,
): ISpecialistsState => {
  switch (action.type) {
    case SET_SEARCH_FILTER:
      return {
        ...state,
        search: action.payload,
      };
    case SET_LIST_STATE:
      return {
        ...state,
        listState: action.payload,
      };
    case RATE_SPECIALIST:
      return {
        ...state,
        list: state.list.map((specialist) =>
          specialist.id === action.payload.specialistId
            ? updateSpecialistRating(specialist, action.payload.ratingValue)
            : specialist,
        ),
      };
    case TOGGLE_IS_FAVORITE:
      return {
        ...state,
        list: state.list.map((specialist) =>
          specialist.id === action.payload
            ? updateSpecialistIsFavorite(specialist)
            : specialist,
        ),
      };
    default:
      return state;
  }
};
