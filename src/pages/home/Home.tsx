import React, { FC } from 'react';
import { useSelector } from 'react-redux';

import SpecialistsHeader from './components/specialists-header';
import SpecialistsList from './components/specialists-list';
import { specialistsSelector } from './state';
import './Home.scss';

const Home: FC = () => {
  const specialists = useSelector(specialistsSelector);

  return (
    <div className="container">
      <SpecialistsHeader specialists={specialists} />
      <SpecialistsList specialists={specialists} />
    </div>
  );
};

export default Home;
