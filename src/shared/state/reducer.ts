import { combineReducers } from 'redux';

import { specialistsReducer } from '@pages/home/state';

const rootReducer = combineReducers({
  specialists: specialistsReducer,
});

export default rootReducer;
