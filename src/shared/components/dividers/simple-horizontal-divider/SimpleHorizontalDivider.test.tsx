import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import SimpleHorizontalDivider from './SimpleHorizontalDivider';

describe('SimpleHorizontalDivider', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <SimpleHorizontalDivider />;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
