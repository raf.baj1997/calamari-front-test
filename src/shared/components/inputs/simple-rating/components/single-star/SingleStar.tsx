import React, { FC } from 'react';

import { getStarSources } from '../../utils';

import './SingleStar.scss';

interface IOwnProps {
  isFullStar: boolean;
  mouseEnterHandler: () => void;
  mouseLeaveHandler: () => void;
  clickHandler: () => void;
}

const SingleStar: FC<IOwnProps> = ({
  isFullStar,
  mouseEnterHandler,
  mouseLeaveHandler,
  clickHandler,
}: IOwnProps) => {
  const { src, srcSet } = getStarSources(isFullStar);
  return (
    <button
      type="button"
      onMouseEnter={mouseEnterHandler}
      onMouseLeave={mouseLeaveHandler}
      onClick={clickHandler}
    >
      <img className="single-star" src={src} srcSet={srcSet} alt="rating" />
    </button>
  );
};

export default SingleStar;
