import { IImageSources } from '@shared/models/common';

import { STAR_SOURCES_STRATEGY } from './models';

export const getStarSources = (isFullStar: boolean): IImageSources =>
  isFullStar
    ? STAR_SOURCES_STRATEGY.FULL_STAR
    : STAR_SOURCES_STRATEGY.EMPTY_STAR;
