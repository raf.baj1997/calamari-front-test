import FullStarIcon from '@assets/icons/star-blue/star-blue.png';
import FullStarIcon2x from '@assets/icons/star-blue/star-blue@2x.png';
import FullStarIcon3x from '@assets/icons/star-blue/star-blue@3x.png';
import EmptyStarIcon from '@assets/icons/star-gray/star-gray.png';
import EmptyStarIcon2x from '@assets/icons/star-gray/star-gray@2x.png';
import EmptyStarIcon3x from '@assets/icons/star-gray/star-gray@3x.png';

import { IImageSources } from '@shared/models/common';

export interface IStarSourcesStrategy {
  FULL_STAR: IImageSources;
  EMPTY_STAR: IImageSources;
}

export const STAR_SOURCES_STRATEGY: IStarSourcesStrategy = {
  FULL_STAR: {
    src: FullStarIcon,
    srcSet: `${FullStarIcon2x} 2x, ${FullStarIcon3x} 3x`,
  },
  EMPTY_STAR: {
    src: EmptyStarIcon,
    srcSet: `${EmptyStarIcon2x} 2x, ${EmptyStarIcon3x} 3x`,
  },
};
