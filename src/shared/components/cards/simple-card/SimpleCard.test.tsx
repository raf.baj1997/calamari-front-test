import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import SimpleCard from './SimpleCard';

const CARD_CONTENT_MOCK = 'card content';

describe('SimpleCard', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <SimpleCard>{CARD_CONTENT_MOCK}</SimpleCard>;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
