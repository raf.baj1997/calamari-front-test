import React, { FC, memo, ReactNode } from 'react';

import { addAdditionalClass } from '@shared/utils/styleOperations';

import './SimpleCard.scss';

interface IOwnProps {
  children: ReactNode;
  additionalClass?: string;
}

const SimpleCard: FC<IOwnProps> = ({
  children,
  additionalClass,
}: IOwnProps) => {
  const className = addAdditionalClass('card-container', additionalClass);
  return <div className={className}>{children}</div>;
};

export default memo(SimpleCard);
