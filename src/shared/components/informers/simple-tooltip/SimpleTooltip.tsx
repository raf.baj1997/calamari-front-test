import React, {
  FC,
  memo,
  ReactNode,
  ReactPortal,
  useEffect,
  useRef,
  useState,
} from 'react';
import ReactDOM from 'react-dom';

import './SimpleTooltip.scss';

interface IOwnProps {
  children: ReactNode;
  render: ReactNode | string;
  className?: string;
}

const SimpleTooltip: FC<IOwnProps> = ({
  children,
  render,
  className,
}: IOwnProps) => {
  const [domNode, setDomNode] = useState<Element | null>(null);
  const [isActive, setIsActive] = useState<boolean>(false);
  const sourceRef = useRef<HTMLSpanElement | null>(null);

  useEffect(() => {
    const nodeId = 'tooltip-wrapper';
    const node = document.querySelector(`#${nodeId}`);
    if (!node) {
      const newNode = document.createElement('div');
      newNode.setAttribute('id', nodeId);
      document.body.appendChild(newNode);
      setDomNode(newNode);
    } else {
      setDomNode(node);
    }
  }, []);

  const renderTooltip = (): ReactPortal | null => {
    if (domNode && isActive) {
      const {
        top,
        left,
        width,
      } = sourceRef.current?.getBoundingClientRect() as any;
      const tooltipDynamicStyle = {
        bottom: window.innerHeight - top - window.scrollY + 8,
        left: left + width / 2 + window.scrollX,
      };
      return ReactDOM.createPortal(
        <div className="tooltip-container" style={tooltipDynamicStyle}>
          <div className="tooltip-container__content">
            {render} <span className="tooltip-container__content__triangle" />
          </div>
        </div>,
        domNode,
      );
    }
    return null;
  };

  const showTooltip = (): void => {
    setIsActive(true);
  };

  const hideTooltip = (): void => {
    setIsActive(false);
  };

  const source = (
    <span
      className={className}
      onMouseEnter={showTooltip}
      onMouseLeave={hideTooltip}
      onFocus={showTooltip}
      onBlur={hideTooltip}
      ref={sourceRef}
      key={0}
    >
      {children}
    </span>
  );
  return <>{[source, renderTooltip()]}</>;
};

export default memo(SimpleTooltip);
