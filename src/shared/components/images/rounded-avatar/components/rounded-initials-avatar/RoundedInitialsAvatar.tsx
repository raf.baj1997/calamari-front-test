import React, { FC, memo } from 'react';

import { getColorsByInitials } from '../../utils';

import './RoundedInitialsAvatar.scss';

interface IOwnProps {
  initials: string;
}

const RoundedInitialsAvatar: FC<IOwnProps> = ({ initials }: IOwnProps) => {
  const colors = getColorsByInitials(initials);
  const { backgroundColor, textColor } = colors;
  return (
    <div className="initials-avatar" style={{ backgroundColor }}>
      <span className="initials-avatar__text" style={{ color: textColor }}>
        {initials}
      </span>
    </div>
  );
};

export default memo(RoundedInitialsAvatar);
