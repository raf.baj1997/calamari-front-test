import {
  IInitialsAvatarColor,
  INITIALS_AVATARS_COLORS_CONFIG,
} from '@shared/models/colors';

const numberFromText = (text: string): number =>
  +text
    .split('')
    .map((char) => char.charCodeAt(0))
    .join('');

export const getColorsByInitials = (initials: string): IInitialsAvatarColor =>
  INITIALS_AVATARS_COLORS_CONFIG[
    numberFromText(initials) % INITIALS_AVATARS_COLORS_CONFIG.length
  ];
