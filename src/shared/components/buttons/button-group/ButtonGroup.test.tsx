import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import ButtonGroup from './ButtonGroup';

const BUTTON_TEXT_MOCK = 'button';

describe('ButtonGroup', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = (
      <ButtonGroup>
        <button type="button">{BUTTON_TEXT_MOCK}</button>
      </ButtonGroup>
    );
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
