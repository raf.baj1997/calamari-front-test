import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import ToggleButton from './ToggleButton';

const BUTTON_TEXT_MOCK = 'button';

describe('ToggleButton', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <ToggleButton>{BUTTON_TEXT_MOCK}</ToggleButton>;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
