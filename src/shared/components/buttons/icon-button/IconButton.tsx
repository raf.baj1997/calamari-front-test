import React, { FC, ReactNode } from 'react';

import { addAdditionalClass } from '@shared/utils/styleOperations';

import './IconButton.scss';

interface IOwnProps {
  children: ReactNode;
  additionalClass?: string;
  clickHandler?: () => void;
}

const IconButton: FC<IOwnProps> = ({
  children,
  additionalClass,
  clickHandler,
}: IOwnProps) => {
  const className = addAdditionalClass('icon-button', additionalClass);
  return (
    <button className={className} type="button" onClick={clickHandler}>
      {children}
    </button>
  );
};

export default IconButton;
