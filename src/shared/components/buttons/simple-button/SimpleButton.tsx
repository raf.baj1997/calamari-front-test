import React, { FC, ReactNode } from 'react';

import { addAdditionalClass } from '@shared/utils/styleOperations';

import './SimpleButton.scss';

interface IOwnProps {
  children: ReactNode;
  additionalClass?: string;
  clickHandler?: () => void;
}

const SimpleButton: FC<IOwnProps> = ({
  children,
  additionalClass,
  clickHandler,
}: IOwnProps) => {
  const className = addAdditionalClass('simple-button', additionalClass);
  return (
    <button className={className} type="button" onClick={clickHandler}>
      {children}
    </button>
  );
};

export default SimpleButton;
