import { shallow } from 'enzyme';
import React, { ReactElement } from 'react';
import renderer from 'react-test-renderer';

import SimpleButton from './SimpleButton';

const BUTTON_TEXT_MOCK = 'button';

describe('SimpleButton', () => {
  let component: ReactElement;

  beforeEach(() => {
    component = <SimpleButton>{BUTTON_TEXT_MOCK}</SimpleButton>;
  });

  it('should be truthy', () => {
    const wrapper = shallow(component);
    expect(wrapper.exists()).toBe(true);
  });

  it('should match snapshot', () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
