export interface IImageSources {
  src: string;
  srcSet: string;
}

export interface IRating {
  value: number;
  votesAmount: number;
}
