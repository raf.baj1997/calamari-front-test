export const addAdditionalClass = (
  baseClass: string,
  additionalClass?: string,
): string => (additionalClass ? `${baseClass} ${additionalClass}` : baseClass);
