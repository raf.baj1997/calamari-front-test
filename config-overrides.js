const { override, addWebpackAlias } = require('customize-cra');
const path = require('path');

module.exports = override(
  addWebpackAlias({
    ['@pages']: path.resolve(__dirname, './src/pages'),
    ['@assets']: path.resolve(__dirname, './src/assets'),
    ['@public']: path.resolve(__dirname, './public'),
    ['@shared']: path.resolve(__dirname, './src/shared'),
  }),
);
